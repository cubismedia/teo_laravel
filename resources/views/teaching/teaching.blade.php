@extends('layout')
@section('content')
    <div class="title">
        <h1>Teaching</h1>
        <img src="/img/page_title_lines.gif" alt="separator" class="arrow">
    </div>
    <div class="teaching-header">
    </div>
    <div class="separator">
        <img src="/img/page_title_lines.gif" alt="separator" class="arrow">
    </div>
    <div class="container bg_fundraiser">
        <div class="row">
            <div class="col-lg-9 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <div class="maiya_text">
                    @foreach($sections as $section)
                        {!! $section->content !!}
                    @endforeach
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container bg_fundraiser">
        <div class="row">
            <div class="teaching_img">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <img src="/img/Maiya-Teaching.jpg" class="img-responsive img_border">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <img src="/img/Maiya-Teaching2.jpg" class="img-responsive img_border">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <img src="/img/Maiya-Teaching3.jpg" class="img-responsive img_border">
                </div>
            </div>
        </div>
    </div>
@endsection
