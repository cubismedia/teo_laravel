<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Maiya Golf</title>
    <meta name="viewechoport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/owl.theme.default.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js') }}"></script>
    <script src="{{ asset('//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/default.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <link href="{{ asset('css/fonts/stylesheet.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/dropdown.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/default.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('js/home.js') }}"></script>
    <script src="{{ asset('//use.typekit.net/kkx4kxa.js') }}"></script>
    <script>try{Typekit.load();}catch(e){}</script>
</head>
<body>
<!--nocache-->

<!--/nocache-->
<header>
    <div class="logo">
        <a href="/">
            <img src="/img/logo_maiya_color.png" class="img-responsive" alt="Maiya Tanaka">
        </a>
    </div>
</header>
<div class="ribbon-back"></div>
<div class="navigation">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-8 col-xs-8">
                <nav class="navbar navbar-inverse">
                    <div class="container">
                        <button class="navbar-toggle float_left" data-toggle="collapse" data-target=".collapse_target">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse collapse_target">
                            <ul class="nav navbar-nav navbar-left">
                                <li class="{{ Request::is('/') ? 'current_page_item' : '' }}"><a href="/" accesskey="1" title="">Home</a></li>
                                <li class="{{ Request::is('maiya') ? 'current_page_item' : '' }}"><a href="/maiya" accesskey="1" title="">Maiya</a></li>
                                <li class="{{ Request::is('teaching') ? 'current_page_item' : '' }}"><a href="/teaching" accesskey="2" title="">Teaching</a></li>
                                <li class="{{ Request::is('events') ? 'current_page_item' : '' }}"><a href="/events" accesskey="3" title="">Events</a></li>
                                <li class="{{ Request::is('photo_gallery') ? 'current_page_item' : '' }}"><a href="/photo_gallery" accesskey="4" title="">Multimedia</a></li>
                                <li class="{{ Request::is('partners') ? 'current_page_item' : '' }}"><a href="/partners" accesskey="5" title="">Partners</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="top_social float_right">
                    <a href="https://twitter.com/MAIYAgolf" target="_blank" class="btn_social twitter"><span class="icon-twitter"></span></a>
                    <a href="http://instagram.com/maiyagolf" target="_blank" class="btn_social twitter"><span class="icon-instagram"></span></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2">
                <div class="ribbon">
                    <img src="/img/ribbon.png" class="img-responsive hidden-xs" alt="Ribbon">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<main>
    @yield('content')
</main>
<div id="bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-4 col-xs-1">
                <a href="/"><img src="/img/logo_maiya_blk.png" alt="Maiya Tanaka" class="img-responsive hidden-xs"></a>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-8 com-xs-11">
                <a href="http://uncommonthinking.com/" target="_blank" alt="Uncommon Thinking">
                    <img src="/img/logo_uncommon.png" class="img-responsive float_right ut">
                </a>
                <nav class="float_right">
                    <span class="{{ Request::is('terms_of_use') ? 'current_page_item' : '' }}"><a href="/terms_of_use" accesskey="2" title="">Terms of Use</a></span>
                    <span class="{{ Request::is('privacy_policy') ? 'current_page_item' : '' }}"><a href="/privacy_policy" accesskey="2" title="">Privacy Policy</a></span>
                    <span class="{{ Request::is('contact') ? 'current_page_item' : '' }}"><a href="/contact" accesskey="2" title="">Contact</a></span>
                </nav>
            </div>
        </div>
    </div>
</div>
<footer></footer>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-58339536-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
