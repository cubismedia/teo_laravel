@extends('layout')
<script src="{{ asset ('js/jquery.easytabs.min.js') }}"></script>
<script src="{{ asset ('js/tabs.js') }}"></script>
<script src="{{ asset ('files/slimbox/js/slimbox2.js') }}"></script>
<link href="{{ '/files/slimbox/css/slimbox2.css' }}" rel="stylesheet">
<link href="{{ '/files/colorbox/colorbox.css' }}" rel="stylesheet">
<script src="{{ asset ('/files/colorbox/jquery.colorbox-min.js') }}"></script>
<script src="{{ asset ('js/video.js') }}"></script>
@section('content')
    <div class="title">
        <h1>Multimedia</h1>
        <img src="/img/page_title_lines.gif" alt="separator" class="arrow">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-xs-8 col-xs-offset-2">
               {{-- @if (!empty($photos))--}}
                <div class="content">
                    <div id="tab-container" class="tab-container">
                        <ul class="etabs">
                            {{--@foreach($photo_categories as $photo_category)
                            <li class="tab"><a href="#{{ $photo_category->slug }}">{{ $photo_category->name }}</a></li>
                            @endforeach--}}
                            <li class="tab"><a href="#ontour">On Tour</a></li>
                            <li class="tab"><a href="#personal">Personal</a></li>
                            <li class="tab"><a href="#videos">Videos</a></li>
                        </ul>
                        <div class="panel-container">
                            @foreach($photos as $photo)
                                {{--<div id="{{ $photo_category->slug }}">--}}
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                        <div class="photo_container">
                                            <a href="/img/uploads/{{ $photo->image_large }}" class="img-responsive" rel="lightbox-1"><img src="/img/uploads/{{ $photo->image_thumb }}" class="img-responsive"></a>
                                        </div>
                                    </div>
                            @endforeach
                        </div>
                        <div class="clear"></div>
                            <div id="videos">
                                {{--@if (!empty($youtube))--}}
                                   {{-- @endif--}}
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
               {{-- @endif--}}
            </div>
        </div>
    </div>
@endsection
