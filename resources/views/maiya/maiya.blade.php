@extends('layout')
@section('content')
    <div class="title">
        <h1>Maiya</h1>
        <img src="/img/page_title_lines.gif" alt="separator" class="arrow">
    </div>
    <div class="container-fluid bg_maiya">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <div class="maiya_text">
                        <img src="/img/Maiya-portrait.jpg" class="img-responsive img_border float_right">
                        @foreach($sections as $section)
                            {!! $section->content !!}
                        <img src="/img/maiya_the_first_tee.jpg" class="img_center">
                        <div class="clear"></div>
                            {!! $section->content !!}
                        <img src="/img/maiya_team.jpg" class="img-responsive maiya_team float_left">
                            {!! $section->content !!}
                        <div class="clear"></div>
                            {!! $section->content !!}
                        <br><br>
                    </div>
                    <img src="/img/maiya_big_break.jpg" class="img-responsive">
                    <div class="maiya_text">
                        <br>
                        {!! $section->content !!}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
