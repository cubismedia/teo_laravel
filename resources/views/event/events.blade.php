@extends('layout')
{{--<script src="{{ asset('js/jquery.infieldlabel.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/register.js') }}"></script>--}}
{{--<link href="{{ asset('css/form.css') }}" rel="stylesheet" />--}}
@section('content')
    <div class="title">
        <h1>Events</h1>
        <img src="/img/page_title_lines.gif" alt="separator" class="arrow">
    </div>
    <div class="container bg_fundraiser">
        <div class="row">
            <div class="col-lg-9 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <div class="maiya_text">
                    <img src="/img/Events.jpg" class="img-responsive img_border float_right">
                    @foreach($sections as $section)
                        {!! $section->content !!}
                    @endforeach
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
