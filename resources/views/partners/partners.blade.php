@extends('layout')
@section('content')
<div class="title">
    <h1>Partners</h1>
    <img src="/img/page_title_lines.gif" alt="separator" class="arrow">
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
            <div class="content">
                {{--@if (!empty($partners))--}}
                @foreach($partners as $partner)
                <div class="col-lg-12">
                    {{--@if (!empty($partner->content))--}}
                    <div class="partner_containers">
                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                            <img src="/img/uploads/{{ $partner->image }}" alt="{{ $partner->name }}" class="img-responsive float_left">
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                            <div class="descriptions">
                                <h3>{{ $partner->name }}</h3>
                                {!! $partner->content !!}

                                <a href="{{ $partner->website }}" target="_blank" class="btns float_left">Visit Website</a>

                                @if($partner->facebook == '')
                                    @else
                                        <a href="{{ $partner->facebook }}" target="_blank" class="btn_sponsors"><span class="icon-facebook"></span></a>
                                @endif
                                @if($partner->twitter == '')
                                    @else
                                        <a href="{{ $partner->twitter }}" target="_blank" class="btn_sponsors large"><span class="icon-twitter"></span></a>
                                @endif
                                @if($partner->pinterest == '')
                                    @else
                                        <a href="{{ $partner->pinterest }}" target="_blank" class="btn_sponsors large"><span class="icon-pinterest"></span></a>
                                @endif
                                @if($partner->instagram == '')
                                    @else
                                        <a href="{{ $partner->instagram }}" target="_blank" class="btn_sponsors large"><span class="icon-instagram"></span></a>
                                @endif
                                @if($partner->google_plus == '')
                                    @else
                                        <a href="{{ $partner->google_plus }}" target="_blank" class="btn_sponsors large"><span class="icon-google"></span></a>
                                @endif
                                @if($partner->youtube == '')
                                    @else
                                        <a href="{{ $partner->youtube }}" target="_blank" class="btn_sponsors large"><span class="icon-youtube"></span></a>
                                @endif
                                @if($partner->linkedin == '')
                                    @else
                                        <a href="{{ $partner->linkedin }}" target="_blank" class="btn_sponsors large"><span class="icon-linkedin"></span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="text">
                    <h2>Sponsorship Inquiries</h2>
                    @foreach($sections as $section)
                   {!! $section->content !!}
                    @endforeach
                    <h3>Xcell Pro Sports Management</h3>
                    <p>P.O. Box 666<br>Rancho Santa Fe, CA 92067<br>
                    <a href="http://www.xcellprosm.com" target="_blank">http://www.XcellProSM.com</a>
                    <img src="/img/xcell_logo.png">
                    <p><span class="icon-phone"></span> 858.922.5699<br>
                        <span class="icon-mail"></span>
                        <a href="mailto:John.Mutch@MVadvisorsllc.com">John.Mutch@MVadvisorsllc.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
