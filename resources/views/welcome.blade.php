@extends ('layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="owl-carousel owl-theme">
                @foreach($hpsliders as $hpslider)
                    <div class="item">
                        <img src="/img/uploads/{{ $hpslider->image }}" class="object-fit-contain">
                        <div class="caption">
                            <h3 class="hp_titles">{{ $hpslider->title }}</h3>
                            <h1 class="hp_sub_titles">{{ $hpslider->subtitle }}</h1>
                            <p>{{ $hpslider->content }}</p>
                           @if($hpslider->link == '')
                               @else
                            <span class="white"><a href="{{ $hpslider->link }}" class="btns" target="{{ $hpslider->target }}">{{ $hpslider->button }}</a></span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container hp_bg">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <!-- Instagram -->
                <div class="title_side">
                    <h2 class="float_left">Instagram</h2>
                    <a href="http://instagram.com/maiyagolf" target="_blank" class="btns float_right">Follow</a>
                    <img src="/img/title_lines.gif" alt="Separator" class="arrow">
                </div>
                <div id="hp_news">
                  @foreach ($instagram as $instagram)
                    <div class="col-lg-3 col-md-3 col-sm-3 col-sm-12">
                        <ul class="instagram_post">
                            <li>
                                <div class="instagram_post">
                                    <div class="instagram">
                                        <a href="{{ $instagram->link }}" target="_blank"><img src="/img/uploads/{{ $instagram->image }}" class="img img-responsive"></a>
                                        <div class="instagram_info">
                                            View Now
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                   @endforeach
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="dark_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    <!-- Instagram Hashtag -->
                    <div class="title_side">
                        <h2 class="float_left"><span class="icon-instagram"></span> @MaiyaGolf <span>on Instagram</span></h2>
                        <a href="http://twitter.com/MAIYAgolf" target="_blank" class="btns float_right">Follow</a>
                        <img src="/img/title_lines_dark.gif" alt="Separator" class="arrow">
                    </div>
                    {{--@if(!empty($twitter))--}}
                    <div id="hp_twitter">
                        <div class="inner">
                            <div class="primary">
                                <div class="slider">
                                    <ul class="hp_twitter">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--@endif--}}
                </div>
            </div>
        </div>
    </div>
@endsection
