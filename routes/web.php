<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/maiya', function () {
    return view('maiya.maiya');
});
Route::get('/teaching', function (){
   return view('teaching.teaching');
});


Route::get('/', 'HpslidersController@show');
//Route::get('/', 'InstagramsController@show');

Route::get('/maiya', 'SectionsController@show');
Route::get('/teaching', 'TeachingController@show');
Route::get('/events', 'EventsController@show');
Route::get('/photo_gallery', 'PhotosController@show');
Route::get('/partners', 'PartnersController@show');
