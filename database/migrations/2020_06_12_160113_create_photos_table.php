<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->string('image_thumb')->nullable();
            $table->string('image_large')->nullable();
            $table->timestamps();
        });

        Schema::create('photo_categories_photo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('photo_category_id');
            $table->unsignedBigInteger('photo_id');
            $table->timestamps();

            $table->unique(['photo_category_id', 'photo_id']);
            $table->foreign('photo_category_id')->references('id')->on('photo_categories')->onDelete('cascade');
            $table->foreign('photo_id')->references('id')->on('photos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
