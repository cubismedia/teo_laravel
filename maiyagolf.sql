-- -------------------------------------------------------------
-- TablePlus 3.5.3(314)
--
-- https://tableplus.com/
--
-- Database: maiyagolf
-- Generation Time: 2020-06-23 20:37:43.7620
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image_thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_large` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `photos` (`id`, `image_thumb`, `image_large`, `created_at`, `updated_at`) VALUES
('5', '5533c8061e479_cp.jpg', '5533c8061e479.jpg', NULL, NULL),
('6', '5533c81568b27_cp.jpg', '5533c81568b27.jpg', NULL, NULL),
('7', '5533c827340d1_cp.jpg', '5533c827340d1.jpg', NULL, NULL),
('8', '5533c8399753f_cp.jpg', '5533c8399753f.jpg', NULL, NULL),
('9', '5533c850ebf60_cp.jpg', '5533c850ebf60.jpg', NULL, NULL),
('10', '5533c87fabfeb_cp.jpg', '5533c87fabfeb.jpg', NULL, NULL),
('11', '5533cae7030f0_cp.jpg', '5533cae7030f0.jpg', NULL, NULL),
('12', '5533cc1e66120_cp.jpg', '5533cc1e66120.jpg', NULL, NULL),
('14', '5533cc31d005b_cp.jpg', '5533cc31d005b.jpg', NULL, NULL),
('15', '5533cc41baa3e_cp.jpg', '5533cc41baa3e.jpg', NULL, NULL),
('16', '5533cc4d8470d_cp.jpg', '5533cc4d8470d.jpg', NULL, NULL),
('17', '5533cc5c8210c_cp.jpg', '5533cc5c8210c.jpg', NULL, NULL),
('18', '5533cc68cae24_cp.jpg', '5533cc68cae24.jpg', NULL, NULL),
('19', '5533cc8825f4d_cp.jpg', '5533cc8825f4d.jpg', NULL, NULL),
('20', '5533cc9a33ace_cp.JPG', '5533cc9a33ace.JPG', NULL, NULL),
('21', '5533cca780f8e_cp.JPG', '5533cca780f8e.JPG', NULL, NULL),
('22', '5533ccb694fc4_cp.jpg', '5533ccb694fc4.jpg', NULL, NULL),
('23', '5533cccad85d3_cp.jpg', '5533cccad85d3.jpg', NULL, NULL),
('24', '5533ccde3b8c3_cp.jpg', '5533ccde3b8c3.jpg', NULL, NULL),
('25', '5533ccea0bb00_cp.jpg', '5533ccea0bb00.jpg', NULL, NULL),
('26', '5533ccfe0998b_cp.jpg', '5533ccfe0998b.jpg', NULL, NULL),
('27', '5533cd0929fc2_cp.jpg', '5533cd0929fc2.jpg', NULL, NULL),
('28', '5eb33cc5cb2a4_cp.jpg', '5eb33cc5cb2a4.jpg', NULL, NULL),
('29', '5eb33cda5bbfc_cp.jpg', '5eb33cda5bbfc.jpg', NULL, NULL),
('30', '5eb33d1e841e5_cp.jpg', '5eb33d1e841e5.jpg', NULL, NULL),
('31', '5eb33d56a4d54_cp.jpg', '5eb33d56a4d54.jpg', NULL, NULL),
('32', '5eb33d9522285_cp.jpg', '5eb33d9522285.jpg', NULL, NULL),
('33', '5eb33dc3b2b15_cp.jpg', '5eb33dc3b2b15.jpg', NULL, NULL),
('34', '5eb33de987034_cp.jpg', '5eb33de987034.jpg', NULL, NULL),
('35', '5eb33e09b4180_cp.jpg', '5eb33e09b4180.jpg', NULL, NULL),
('36', '5eb33e2dce859_cp.jpg', '5eb33e2dce859.jpg', NULL, NULL),
('37', '5eb33e7f55a74_cp.jpg', '5eb33e7f55a74.jpg', NULL, NULL),
('39', '5ec5f48b42fc8_cp.jpg', '5ec5f48b42fc8.jpg', NULL, NULL);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;