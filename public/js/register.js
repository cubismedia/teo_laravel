// JavaScript Document

$(function(){ $("label").inFieldLabels(); });

$(document).ready(function(e){

	//Dropdown
	$("select").msDropdown();

    //Check/Uncheck Radio Button
    var allRadios = $('[name="data[Registration][sponsor_type]"]');
    var booRadio;
    var x = 0;
    for(x = 0; x < allRadios.length; x++){

        allRadios[x].onclick = function() {
            if(booRadio == this){
                this.checked = false;
                booRadio = null;
            }else{
                booRadio = this;
            }
        };
    }

    /**
     * Sponsor content display
     */
    if ($("[name='data[Registration][sponsor_type]']").is(':checked')) {
        $('#sponsor_content').show();
    } else {
        $('#sponsor_content').hide();
    }
    /**
     * Toggle Sponsor Content
     */
    $(function() {
        $("[name='data[Registration][sponsor_type]']").click(function(){
            if ($(this).is(':checked')) {
                $('#sponsor_content').slideDown();
            } else {
                $('#sponsor_content').slideUp();
            }
        });
    });
});




