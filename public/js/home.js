$(document).ready(function(){
	

	//BxSlider
	$('.hp_twitter').bxSlider({
		mode: 'horizontal',
		slideWidth: 290,
		minSlides: 3,
		maxSlides: 3,
		slideMargin: 50,
		moveSlides: 3,
		pager: true,
		controls: false
	});
	//Init the carousel
	$(".owl-carousel").owlCarousel( {
		loop:true,
		autoplay: true,
		autoplayTimeout: 7000,
		smartSpeed: 600,
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	});
	
});