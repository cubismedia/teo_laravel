<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function photo_category()
    {
        return $this->belongsTo('App\PhotoCategory');
    }
}
