<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoCategory extends Model
{
    public function photo()
    {
        return $this->hasMany('App\Photo');
    }
}
