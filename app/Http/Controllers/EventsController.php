<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function show()
    {
        $sections = Section::
        where('name', 'events')
            ->orderBy('index', 'asc')
            ->get();
        return view('event.events', ['sections' => $sections]);
    }
}
