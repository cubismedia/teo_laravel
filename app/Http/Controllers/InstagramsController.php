<?php

namespace App\Http\Controllers;
use App\Instagram;
use Illuminate\Http\Request;

class InstagramsController extends Controller
{
    public function show()
    {
        $instagram = Instagram::all();
        return view('welcome', ['instagram' => $instagram]);
    }
}
