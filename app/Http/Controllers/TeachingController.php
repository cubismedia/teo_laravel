<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;

class TeachingController extends Controller
{
    public function show()
    {
        $sections = Section::
        where('name', 'teaching')
            ->orderBy('index', 'asc')
            ->get();
        return view('teaching.teaching', ['sections' => $sections]);
    }
}
