<?php

namespace App\Http\Controllers;
use App\Photo;
use App\PhotoCategory;
use Illuminate\Http\Request;

class PhotoCategoriesController extends Controller
{
    public function show()
    {
        $photo_categories = PhotoCategory::orderBy('ordering_position', 'asc')->get();
        return view('photo_gallery.photo_gallery', ['photo_categories' => $photo_categories]);

    }
}
