<?php

namespace App\Http\Controllers;
use App\Hpslider;
use App\Photo;
use App\PhotoCategory;
use Illuminate\Http\Request;

class PhotosController extends Controller
{
    public function show()
    {
        $photos = Photo::orderBy('image_thumb', 'asc')->get();
        return view('photo_gallery.photo_gallery', ['photos' => $photos]);
    }
}
