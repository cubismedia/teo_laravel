<?php

namespace App\Http\Controllers;
use App\Partner;
use App\Section;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    public function show()
    {
        $partners = Partner::orderBy('ordering_position', 'asc')->get();
        $sections = Section::where('name', 'sponsors')->get();
        return view('partners.partners', ['partners' => $partners], ['sections' => $sections]);

    }

}
