<?php

namespace App\Http\Controllers;
use App\Section;
use Illuminate\Http\Request;

class SectionsController extends Controller
{
    public function show()
    {
        $sections = Section::
        where('name', 'maiya')
            ->orderBy('index', 'asc')
            ->get();
        return view('maiya.maiya', ['sections' => $sections]);
    }

}
