<?php

namespace App\Http\Controllers;
use App\Hpslider;
use App\Instagram;
use Illuminate\Http\Request;

class HpslidersController extends Controller
{
    public function show()
    {
        $hpsliders = Hpslider::orderBy('ordering_position', 'asc')->get();
        $instagram = Instagram::all();
        return view('welcome', ['hpsliders' => $hpsliders], ['instagram' => $instagram]);
    }
}
